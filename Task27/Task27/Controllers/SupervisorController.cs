﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task27.Models;

namespace Task27.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }


        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetAllSupervisors()                // Return all supervisors
        {
            return _context.Supervisors;
        }


        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetSpecificSupervisorById(int id)              // Return a specific supervisor
        {
            var supervisor = _context.Supervisors.Find(id);
            return supervisor;
        }


        [HttpPost]
        public ActionResult<Supervisor> AddSupervisor(Supervisor sup)                  // Add a supervisor
        {
            _context.Add(sup);
            _context.SaveChanges();
            return CreatedAtAction("GetSpecificSupervisorById", new Supervisor { Id = sup.Id }, sup);
        }


        [HttpDelete("{id}")]                                                           
        public ActionResult<Supervisor> DeleteSupervisor(int id)                      // Delete a supervisor
        {
            Supervisor sup = _context.Supervisors.Find(id);

            if (sup == null)
            {
                return NotFound();
            }

            _context.Supervisors.Remove(sup);
            _context.SaveChanges();

            return sup;
        }
        

        [HttpPut("{id}")]                                                            
        public ActionResult<Supervisor> UpdateSupervisor(int id, Supervisor sup)      // Update a supervisor
        {

            if (id != sup.Id)
            {
                return BadRequest();
            }
            _context.Entry(sup).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
    }
}